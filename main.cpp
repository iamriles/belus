// Link statically with GLEW
#define GLEW_STATIC

// Headers
#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <iostream>
#include <chrono>
#include <cmath>

// Shader sources
const GLchar* vertexSource = R"glsl(
    #version 150 core

    in vec2 position;
    in vec3 color;

    out vec3 Color;

    void main()
    {
        Color = color;
        gl_Position = vec4(position, 0.0, 1.0);
    }
)glsl";

const GLchar* fragmentSource = R"glsl(
    #version 150 core

    uniform vec3 multiCol;

    in vec3 Color;

    out vec4 outColor;

    void main()
    {
        vec3 newCol = Color + multiCol;
        outColor = vec4(newCol, 1.0);
    }
)glsl";

int main(){
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  sf::Window window(
                    sf::VideoMode(800, 600, 32),
                    "OpenGL", sf::Style::Titlebar | sf::Style::Close,
                    settings
                    );

  // Initialize GLEW
  glewExperimental = GL_TRUE;
  glewInit();

  // Create Vertex Array Object
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Create a Vertex Buffer Object and copy the vertex data to it
  GLuint vbo;
  glGenBuffers(1, &vbo);

  GLfloat vertices[] = {
                        0.0f,  0.5f, 1.0f, 0.0f, 0.0f,
                        0.4f, 0.0f, 0.0f, 1.0f, 0.0f,
                        -0.4f, 0.0f, 0.0f, 0.0f, 1.0f,
                        0.0f,  -0.5f, 1.0f, 0.0f, 0.0f
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(
               GL_ARRAY_BUFFER,
               sizeof(vertices),
               vertices,
               GL_STATIC_DRAW
               );

  // Create an Element Buffer Object as well as element data
  GLuint ebo;
  glGenBuffers(1, &ebo);

  GLuint elements[] = {
                       0, 1, 2,
                       2, 3, 1
  };

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(
               GL_ELEMENT_ARRAY_BUFFER,
               sizeof(elements),
               elements,
               GL_STATIC_DRAW
               );

  // Create and compile the vertex shader
  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource, NULL);
  glCompileShader(vertexShader);
  std::cout
    << "Vertex shader: "
    << gluErrorString(glGetError())
    << std::endl;

  // Create and compile the fragment shader
  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
  glCompileShader(fragmentShader);
  std::cout
    << "Fragment shader: "
    << gluErrorString(glGetError())
    << std::endl;

  // Link the vertex and fragment shader into a shader program
  GLuint shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glBindFragDataLocation(shaderProgram, 0, "outColor");
  glLinkProgram(shaderProgram);
  glUseProgram(shaderProgram);

  std::cout
    << "Use Program: "
    << gluErrorString(glGetError())
    << std::endl;


  GLint uniColor = glGetUniformLocation(shaderProgram, "multiCol");
  glUniform3f(uniColor, 0.1f, 0.1f, 0.1f);

  // Specify the layout of the vertex position data
  GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(
                        posAttrib,
                        2,
                        GL_FLOAT,
                        GL_FALSE,
                        5*sizeof(float),
                        0
                        );

  // Specify the layout of the vertex color data
  GLint colorAttrib = glGetAttribLocation(shaderProgram, "color");
  glEnableVertexAttribArray(colorAttrib);
  glVertexAttribPointer(
                        colorAttrib,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        5*sizeof(float),
                        (void*)(2*sizeof(float))
                        );


  auto t_start = std::chrono::high_resolution_clock::now();

  bool running = true;
  while (running){
    // Window closing
    sf::Event windowEvent;
    while (window.pollEvent(windowEvent)){
      switch (windowEvent.type){
      case sf::Event::Closed:
        running = false;
        break;
      }
    }


    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    auto t_now = std::chrono::high_resolution_clock::now();
    float time =
      std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start)
      .count();

    glUniform3f(
                uniColor,
                ((sin(time * 7.0f) + 1.0f) / 2.0f), // red
                ((sin(time * 3.0f) + 1.0f) / 2.0f), // green
                ((sin(time * 1.0f) + 1.0f) / 2.0f) // blue
                );

      // Draw a triangle from the 3 vertices
    glDrawElements(
                   GL_TRIANGLES,
                   6,
                   GL_UNSIGNED_INT,
                   0
                   );

    // Swap buffers
    window.display();
  }

  glDeleteProgram(shaderProgram);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);

  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ebo);

  glDeleteVertexArrays(1, &vao);
  window.close();
  return 0;
}
